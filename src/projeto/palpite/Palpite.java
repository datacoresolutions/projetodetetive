package projeto.palpite;

import java.util.Random;
import java.util.Scanner;

public class Palpite {

	protected void criaPalpite(Scanner scan, String[] suspeitos, String[] locais, String[] armas, String[] palpite) {

		System.out.println("Escolha o Suspeito");
		System.out.println("1- Charles B. Abbage  2- Donald Duck Knuth  3- Ada L. Ovelace  4- Alan T. Uring "
				+ " 5- Ivar J. Acobson  6- Ras Mus Ler Dorf");
		System.out.print("Suspeito: ");
		palpite[0] = suspeitos[scan.nextInt() - 1];
		System.out.println("Escolha o Local");
		System.out.println("1- Redmond  2- Palo Alto  3- San Francisco  4- Tokio  5- Restaurante no Fim do Universo "
				+ " 6- S�o Paulo  7- Cupertino  8- Helsinki  9- Maida Vale  10- Toronto");
		System.out.print("Local: ");
		palpite[1] = locais[scan.nextInt() - 1];
		System.out.println("Escolha a arma: ");
		System.out.println("1- Peixeira  2- DynaTAC 8000X (o primeiro aparelho celular do mundo)  "
				+ "3- Trezoit�o  4- Trebuchet  5- Ma�a  6- Gl�dio");
		System.out.println("Arma: ");
		palpite[2] = armas[scan.nextInt() - 1];
	}

//	public String validaPalpite(String cenarioItem) {
//		int posicao = 0;
//		
//		
//				
//		return null;
//
//	}

	protected void testaPalpite(Random random, String[] cenario, String[] palpite, String[] mensagemErro,
			boolean acertou) {

		if (cenario[0] == palpite[0] && cenario[1] == palpite[1] && cenario[2] == palpite[2]) {
			acertou = true;
			System.out.println("Voce Acertou!");
		}

		for (int i = 0; i < palpite.length; i++) {
			if (cenario[i] != palpite[i]) {
				System.out.println(mensagemErro[i]);
			    System.exit(0);
			}
		}

	}
}
