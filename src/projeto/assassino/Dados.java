package projeto.assassino;

import java.util.Random;
import java.util.Scanner;

import projeto.palpite.Palpite;

public class Dados extends Palpite {

	public void descobreAssassino() {
		Scanner scan = new Scanner(System.in);

		String[] suspeitos = { "Charles B. Abbage", "Donald Duck Knuth", "Ada L. Ovelace", "Alan T. Uring",
				"Ivar J. Acobson", "Ras Mus Ler Dorf" };

		String[] locais = { "Redmond", "Palo Alto", "San Francisco", "Tokio", "Restaurante no Fim do Universo",
				"S�o Paulo", "Cupertino", "Helsinki", "Maida Vale", "Toronto" };

		String[] armas = { "Peixeira", "DynaTAC 8000X (o primeiro aparelho celular do mundo)", "Trezoit�o", "Trebuchet",
				"Ma�a", "Gl�dio" };

		String[] cenario = new String[3];
		String[] palpite = new String[3];
		String[] mensagemErro = { "Voce errou o suspeito", "Voce errou o local", "Voce errou a arma" };

		boolean acertou = false;

		Random random = new Random();

		cenario[0] = suspeitos[random.nextInt(suspeitos.length)];
		cenario[1] = locais[random.nextInt(locais.length)];
		cenario[2] = armas[random.nextInt(armas.length)];

		while (!acertou) { // Enquanto nao acertou
			criaPalpite(scan, suspeitos, locais, armas, palpite);

			testaPalpite(random, cenario, palpite, mensagemErro, acertou);
		}
		scan.close();
	}

	
}
